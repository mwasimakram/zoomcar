import { SAVE_LOCATION,SET_BUTTON_ON_MAP,SAVE_ST_DT ,SAVE_END_DT,SET_DT_BTN} from "../Constant";

export function ZoomCarReducer(state:any,action:any){
    const {type,payload}  =action ;
    console.log("reducer callsed")
    switch(type){
        case SAVE_LOCATION:
            return {
                ...state,
                selectedLocation:payload
            }
        case SET_BUTTON_ON_MAP:
            return {
                ...state,
                isMapButton:payload 
            }
        case SAVE_ST_DT:
            return{
                ...state,
                stDateTime:payload
            }
        case SAVE_END_DT:{
            return{
                ...state,
                endDateTime:payload
            }
        }
        case SET_DT_BTN:{
            return{
                ...state,
            isDtBtn:payload
            }
        }
    }
}