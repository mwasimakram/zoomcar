export const HOME_PAGE = "/";
export const LOCATION_PAGE = "/locationPage";
export const MAP_PAGE = "/map";
export const DT_PATE = "/dt";
export const DEFAULT_LOCATIO = {
    lat: 37.42216,
    lng: -122.08427,
};
export const SAVE_LOCATION = "SAVE_LOCATION";
export const SET_BUTTON_ON_MAP = "SET_BUTTON_ON_MAP";
export const SAVE_END_DT = "SAVE_END_DT";
export const SAVE_ST_DT = "SAVE_ST_DT";
export const SET_DT_BTN = "SET_DT_BTN"
export const GOOGLE_KEY = "AIzaSyC6XFxBKc74FNPdp2aXEg0IquKP24L8DhY";
export const GET_ADDRESS_API = `https://maps.googleapis.com/maps/api/geocode/json?`;