import { Layout } from "antd";
import "./App.css";
import { Headers } from "./Component/Common/Headers";
import { Router } from "./Routes/Router";
import ZoomCarContextProvide from "./Context/ZoomcarContext";
const { Header, Footer, Sider, Content } = Layout;

function App() {
  return (
    <ZoomCarContextProvide>
      <Layout>
        <Content>
          <Router />
        </Content>
      </Layout>
    </ZoomCarContextProvide>
  );
}

export default App;
