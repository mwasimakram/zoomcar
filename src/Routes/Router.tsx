import { Routes,Route } from "react-router-dom";
import { Home } from "../Component/Content/Home";
import { Location } from "../Component/Content/Locations/index"
import { HOME_PAGE, LOCATION_PAGE,MAP_PAGE,DT_PATE } from "../Constant";
import {Map} from "../Component/Content/Locations/Map"
import { ChooseDate } from "../Component/Content/ChooseDate";
export function Router(){
    return(
        <Routes>
            <Route path={HOME_PAGE} element={<Home />}></Route>
            <Route path={LOCATION_PAGE} element={<Location />}></Route>
            <Route path={MAP_PAGE} element ={< Map/>}></Route>
            <Route path={DT_PATE} element={<ChooseDate/>}></Route>
        </Routes>
    )
}