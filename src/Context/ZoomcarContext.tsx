import { createContext, useReducer } from "react";
import { ZoomCarReducer } from "../Reducer/Reducer";
interface IContext {
  state: any;
  dispatch: React.Dispatch<any>;
}
const defaultState = {
  state: {},
  dispatch: () => null,
};
const initialState = {
    selectedLocation:"",
    isMapButton:true,
    stDateTime:"",
    endDateTime:"",
    isDtBtn:true
}
export const ZoomCarContext = createContext<IContext>(defaultState);
const ZoomCarContextProvide = (props: any) => {
  const [state, dispatch] = useReducer(ZoomCarReducer,initialState);
  return (
    <ZoomCarContext.Provider value={{ state, dispatch }}>
      {props.children}
    </ZoomCarContext.Provider>
  );
};

export default ZoomCarContextProvide;
