import { Col, Container, Row } from "react-bootstrap";
import { Banner } from "./Banner";
import { MainPageForm } from "./MainPageForm";
import { Headers } from "../Common/Headers";
export function BgPage() {
  return (
    <>
      <Headers />
      <div className="main">
        <MainPageForm />
      </div>
      <div className="">
        <Banner />
      </div>
    </>
  );
}
