import { ArrowRightOutlined } from "@ant-design/icons";
import { Button, Container } from "react-bootstrap";
import { DatePicker, Space } from "antd";
import type { RangePickerProps } from "antd/es/date-picker";
import moment from "moment";
import { useContext } from "react";
import { ZoomCarContext } from "../../Context/ZoomcarContext";
import { SAVE_END_DT, SAVE_ST_DT, SET_DT_BTN } from "../../Constant";
const { RangePicker } = DatePicker;

export function ChooseDate() {
  const { state, dispatch } = useContext(ZoomCarContext);

  const getDateTime = (e: any) => {
    let startDt = e[0]._d.toString().split("GMT")[0];
    let endDt = e[1]._d.toString().split("GMT")[0];
    dispatch({ type: SAVE_ST_DT, payload: startDt });
    dispatch({ type: SAVE_END_DT, payload: endDt });
    dispatch({ type: SET_DT_BTN, payload: false });
  };
  return (
    <>
      <div className="dt">
        <div className="back-to-home">
          <div className="heading">
            <div className="icon"></div>
            <h2>Choose Trip Date</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-2"></div>
          <div className="col-lg-8 col-xs-12">
            <div className="time-bar">
              <div className="select-time">
                <div className="time-box">
                  <div>
                    <div className="date">{state.stDateTime}</div>{" "}
                    <div className="time">{}</div>
                  </div>
                  <div className="divider">
                    <ArrowRightOutlined />{" "}
                  </div>
                  <div className="time-right">
                    <div className="date">{""}</div>{" "}
                    <div className="time">11:00AM</div>
                  </div>
                </div>
              </div>
              <Button className="clear-btn">Clear</Button>
            </div>
            <div className="calender">
              <RangePicker
                ranges={{
                  Today: [moment(), moment()],
                  "This Month": [
                    moment().startOf("month"),
                    moment().endOf("month"),
                  ],
                }}
                showTime={{
                  hideDisabledOptions: true,
                  defaultValue: [
                    moment("00:00:00", "HH:mm A"),
                    moment("11:59:59", "HH:mm: A"),
                  ],
                }}
                format="YYYY-MM-DD HH:mm A"
                onOk={getDateTime}
              />
            </div>
            <div className="button-wrapper">
              <Button
                className={
                  state && state.isDtBtn
                    ? "continue-btn-disable"
                    : "continue-btn"
                }
                disabled={state.isDtBtn}
              >
                CONTINUE
              </Button>
            </div>
          </div>
          <div className="col-lg-2"></div>
        </div>
      </div>
    </>
  );
}
