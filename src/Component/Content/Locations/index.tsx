import { Col, Container, Row } from "react-bootstrap";

import { Button } from "antd";
import { LocationOnMap } from "./LocationOnMap";
import { CurrentLocation } from "./CurrentLocation";
import { GetLocation } from "./GetLocation";
import "./Location.css";
import { BackArrow } from "../../Common/BackArrow";
import { useNavigate } from "react-router-dom";
import { DT_PATE } from "../../../Constant";
import { useContext } from "react";
import { ZoomCarContext } from "../../../Context/ZoomcarContext";
export function Location() {
  const { state, dispatch } = useContext(ZoomCarContext);

  const navigate = useNavigate();
  return (
    <div className="container-fluid pt-2 location">
      <BackArrow />
      <Row className="pt-5" gutter={[16, 16]}>
        <div className="col-lg-1 col-xs-0"></div>
        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <GetLocation />
        </div>
        <div className="col-lg-2 col-md-5 col-sm-6 col-xs-12">
          <CurrentLocation />
        </div>
        <div className="col-lg-2 col-md-5 col-sm-6 col-xs-12">
          <LocationOnMap />
        </div>
        <div className="col-lg-1 col-xs-0"></div>
      </Row>
      <Row>
        <div className="col-lg-1"></div>
        <div className="col-lg-10 col-xs-12 suggested">
          <div className="suggested-box">
            <h3>SUGGESTED PICK UP LOCATIONS</h3>
          </div>
        </div>
        <div className="col-lg-1"></div>
      </Row>
      <Row>
        <div>
          <Button
            className={"pickUp-Btn " + (state && state.isMapButton ? 'disable':'active')}
            disabled={state && state.isMapButton}
            onClick={() => navigate(DT_PATE)}
          >
            CONFIRM PICKUP LOCATION
          </Button>
        </div>
      </Row>
    </div>
  );
}
