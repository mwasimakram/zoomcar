import { useContext } from "react";
import { ZoomCarContext } from "../../../Context/ZoomcarContext";
export function GetLocation() {
  const { state, dispatch } = useContext(ZoomCarContext);

  return (
    <div className="location-box">
      <div className="border-box">
        <div className="field">
          <div className="field-inbox">
            <div className="dot"></div>
            <input
              type="text"
              placeholder="Select your starting point"
              className="input-text"
              value={state.selectedLocation}
            ></input>
          </div>
        </div>
      </div>
    </div>
  );
}
