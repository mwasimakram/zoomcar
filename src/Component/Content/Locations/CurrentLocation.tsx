import { AimOutlined } from "@ant-design/icons";
import {GetAddress} from "../../Axios/Axios"
import { useContext } from "react";
import {ZoomCarContext} from "../../../Context/ZoomcarContext";
import { SAVE_LOCATION, SET_BUTTON_ON_MAP } from "../../../Constant";
export function CurrentLocation() {
  const {state , dispatch} = useContext(ZoomCarContext);
  const getCurrentLocation = async ()=>{
     navigator.geolocation.getCurrentPosition(async function(position) {
      const result = await GetAddress({lat: position.coords.latitude,lng:position.coords.longitude});
      console.log(result);
      dispatch({type:SAVE_LOCATION,payload:result.formatted_address});
      dispatch({type:SET_BUTTON_ON_MAP,payload:false})
    });
  }
  return (
    <div className="local-location" onClick={getCurrentLocation}>
      <div className="location-boxs">
        <AimOutlined className="icon" />
        <div className="ms-2">Current Location</div>
      </div>
    </div>
  );
}
