import { AimOutlined, GlobalOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import { MAP_PAGE } from "../../../Constant";

export function LocationOnMap() {
  const navigate = useNavigate();
  const moveToMap = ()=>{
    navigate(MAP_PAGE)
  }
  return (
    <div className="local-location" onClick={()=>moveToMap()}>
      <div className="location-boxs">
        <GlobalOutlined className="icon " />
        <div className="ms-2">Select Location on Map</div>
      </div>
    </div>
  );
}
