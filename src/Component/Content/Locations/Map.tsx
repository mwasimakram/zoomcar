import { Button, Row } from "antd";
import GoogleMapReact from "google-map-react";
import { DEFAULT_LOCATIO, GOOGLE_KEY,HOME_PAGE,SAVE_LOCATION, SET_BUTTON_ON_MAP } from "../../../Constant";
import { SearchOutlined } from "@ant-design/icons";
import { BackArrow } from "../../Common/BackArrow";
import { useContext } from "react";
import {ZoomCarContext} from "../../../Context/ZoomcarContext";
import {GetAddress} from "../../Axios/Axios"
import { useNavigate } from "react-router-dom";
interface latlng{
  lat:Number,
  lng:Number
}
export function Map() {
  const {state , dispatch} = useContext(ZoomCarContext);
  const navigate  = useNavigate();
  const zoomLevel: any = 11;
  const clickOnMap = async (e:latlng)=>{
    const {lat,lng} = e;
    const result = await GetAddress({lat:lat,lng:lng});
    console.log(result);
    dispatch({type:SAVE_LOCATION,payload:result.formatted_address});
    dispatch({type:SET_BUTTON_ON_MAP,payload:false})

  }
  return (
    <>
      <BackArrow />
      <Row className="addressBar-map">
        <div className="icon">
          <SearchOutlined />
        </div>
        <span className="address">{state.selectedLocation}</span>
      </Row>
      <div className="map">
        <GoogleMapReact
          bootstrapURLKeys={{ key: GOOGLE_KEY }}
          defaultCenter={DEFAULT_LOCATIO}
          defaultZoom={zoomLevel}
          onClick = {clickOnMap}
        ></GoogleMapReact>
        <Row>
          <div className="footer bottom-button">
            <Button className={"continue " + (state && state.isMapButton ? 'disable':'active')} disabled={state && state.isMapButton} onClick={()=>{navigate(HOME_PAGE)}}>CONTINUE</Button>
          </div>
        </Row>
      </div>
    </>
  );
}
