import { Button, Checkbox, Form, Input } from "antd";
import { Col, Container, Row } from "react-bootstrap";

import { SwapOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import { LOCATION_PAGE } from "../../Constant";
import { useContext } from "react";
import { ZoomCarContext } from "../../Context/ZoomcarContext";

export function MainPageForm() {
  const { state, dispatch } = useContext(ZoomCarContext);

  const navigate = useNavigate();
  const moveToLocationPage = () => {
    navigate(LOCATION_PAGE);
  };
  return (
    <Row>
      <Col span={9}></Col>
      <Col span={6}>
        <div className="form">
          <div className="form-item">
            <div className="form-item-container">
              <div className="form-item-text">
                <div className="form-item-btn">
                  <Button type="primary" className="active ">
                    <SwapOutlined />
                    Round Trip
                  </Button>
                </div>
              </div>
            </div>
          </div>
          <div className="form-item-box" onClick={() => moveToLocationPage()}>
            <div className="field-wrapper">
              <div className="address-field">
                <div className="green-dot"></div>
                <div className="placeholders">
                  {" "}
                  {state.selectedLocation || "Pick Up City, Airport, Address or Hotel"}
                </div>
              </div>
            </div>
          </div>
          {state.stDateTime && <div className="form-item-box" onClick={() => moveToLocationPage()}>
            <div className="field-wrapper">
              <div className="address-field">
                <div className="placeholders">
                  {" "}
                  {state.stDateTime + "---->" + state.endDateTime}
                </div>
              </div>
            </div>
          </div>}
          <div className="form-item-last">
            <Button
              className={
                "disableBtn " +
                (state && state.isMapButton ? "disable" : "active")
              }
              disabled={state && state.isMapButton}
            >
              Finds Cars
            </Button>
          </div>
        </div>
      </Col>
      <Col span={9}></Col>
    </Row>
  );
}
