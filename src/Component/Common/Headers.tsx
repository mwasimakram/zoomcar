import { Navbar, Container, Nav } from "react-bootstrap";
import "./Common.css";
export function Headers() {
  return (
    <Navbar bg="dark" expand="lg" className="header">
      <Container>
        <Navbar.Brand href="#" className="logo"></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
          <Navbar.Text>
            <Nav>
              <a href="#" className="host">Become A Host</a>
              <Nav.Link href="#" className="zms">ZMS</Nav.Link>
              <Nav.Link href="#" className="login">Login/Signup</Nav.Link>
            </Nav>
          </Navbar.Text>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
