import { useNavigate } from "react-router-dom";
import { HOME_PAGE } from "../../Constant";
import {ArrowLeftOutlined} from "@ant-design/icons";
import { Col, Container, Row } from "react-bootstrap";
import "./Common.css"
export function BackArrow(){
    const navigate = useNavigate();
  const returnToHomePage = () => {
    navigate(HOME_PAGE);
  };
    return(
        <Row className="row back-arrow">
        <ArrowLeftOutlined
          className="ms-2"
          onClick={() => returnToHomePage()}
        />
      </Row>
    )
}