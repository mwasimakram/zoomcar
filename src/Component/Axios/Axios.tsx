import axios from "axios";
import { GET_ADDRESS_API,GOOGLE_KEY } from "../../Constant";
interface laglng{
    lat:Number,
    lng:Number
}
export async function GetAddress(obj:laglng){
    try {
        const result = await axios.get(`${GET_ADDRESS_API}latlng=${obj.lat},${obj.lng}&key=${GOOGLE_KEY}`);
        if(result.data.error_message){
            return {"formatted_address":"Sorry couldn't fetch"}
        }
        return result.data;
    } catch (error) {
        return error;
    }
}